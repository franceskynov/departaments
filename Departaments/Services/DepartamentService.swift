//
//  DepartamentService.swift
//  Departaments
//
//  Created by Dev on 11/1/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

class DepartamentService: NSObject {
    
    static let header = [
        "Content-Type": "application/json; charset=utf-8"
    ]
    
    static func getDepartaments(completation: @escaping ([Departament]) -> Void) {
        Alamofire.request(Constansts.apiUrl + "departaments", method: .get).responseJSON { (response) in
            
            var departaments = [Departament]()
            
            if response.result.error == nil {
                
                guard let data = response.data else { return }
                
                do {
                    if let json = try JSON(data: data).array {
                        print(json)
                        for item in json {
                            
                            let id = item["id"].intValue
                            let name = item["name"].stringValue
                            let zone =  item["zone"].stringValue
                            var municipalities =  [Municipality]()
                            
                            for municipality in item["municipalities"].arrayValue {
                                municipalities.append(Municipality(id: municipality["id"].intValue,
                                                                   name: municipality["name"].stringValue)
                                )
                            }
                            
                           departaments.append(Departament(id: id, name: name, zone: zone, municipalities: municipalities))
                        }
                        completation(departaments)
                    }
                } catch {
                    debugPrint(error)
                }
                
            } else {
                completation(departaments)
            }
        }
    }
}

