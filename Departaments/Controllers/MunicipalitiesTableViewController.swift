//
//  MunicipalitiesTableViewController.swift
//  Departaments
//
//  Created by Dev on 11/1/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class MunicipalitiesTableViewController: UITableViewController {

    var municipalities = [Municipality]()
    var indexOfSelectedMunicipality: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return municipalities.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "municipalitiesCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MunicipalityTableViewCell else {
            fatalError("Cannot load the cell")
        }
        
        let municipality: Municipality?
        municipality = municipalities[indexPath.row]
        cell.municipalityLabel.text = municipality?.name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexOfSelectedMunicipality = indexPath.row
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    
        
        /*if let myIdentifier = segue.identifier {
            if myIdentifier == "municipalityDetail" {
                let details = segue.destination as? MunicipalityViewController
                details?.municipality = municipalities[indexOfSelectedMunicipality!]
                
            }
        }*/
        
        switch (segue.identifier ?? "") {
        case "municipalityDetail":
            guard let municipalitiesControllers = segue.destination as? MunicipalityViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            
            guard let selectedCell = sender as? MunicipalityTableViewCell else {
                fatalError("Unexpected sender")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selectd cell is ...")
            }
            
            let selectedMunicipality: Municipality?
            
            selectedMunicipality = municipalities[indexPath.row]
            
            municipalitiesControllers.municipality = selectedMunicipality
            break
            
        default:
            fatalError("Unexpected segue")
        }
    }
 

}
