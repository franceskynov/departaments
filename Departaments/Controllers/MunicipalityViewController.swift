//
//  MunicipalityViewController.swift
//  Departaments
//
//  Created by Dev on 11/6/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class MunicipalityViewController: UIViewController {

    var municipality: Municipality?
    @IBOutlet weak var municipalityLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let municipality = municipality {
            municipalityLabel.text = municipality.name
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
