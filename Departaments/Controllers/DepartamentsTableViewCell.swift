//
//  DepartamentsTableViewCell.swift
//  Departaments
//
//  Created by Dev on 11/1/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class DepartamentsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var departamentLabel: UILabel!
    @IBOutlet weak var zoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
