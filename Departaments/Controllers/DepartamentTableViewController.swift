//
//  DepartamentTableViewController.swift
//  Departaments
//
//  Created by Dev on 11/1/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class DepartamentTableViewController: UITableViewController{

    @IBOutlet var searchFooter: SearchFooter!
    var departaments = [Departament]()
    var filteredDepartaments = [Departament]()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Buscar departamentos"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        tableView.tableFooterView = searchFooter
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        //departaments.append(Departament(id: 1, name: "San salvador", zone: "Zona central", municipalities: [ Municipality(id: 1, name: "San salvador"), Municipality(id: 2, name: "Cuscatancingo") ]))
        DepartamentService.getDepartaments { (departaments) in
            self.departaments = departaments
            self.tableView.reloadData()
        }
        
    
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isFiltering() {
            //searchFooter.setIsFilteringToShow(filteredItemCount: filteredDepartaments.count, of: departaments.count)
            return filteredDepartaments.count
        }
        
        //searchFooter.setNotFiltering()
        
        return departaments.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "departamentCell", for: indexPath)

        //cell.textLabel?.text = ""

        
        
        let cellIdentifier = "departamentCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DepartamentsTableViewCell else {
            fatalError("Cannot load the cell")
        }
        
        let departament: Departament?
        
        if isFiltering() {
            departament = filteredDepartaments[indexPath.row]
        } else {
            departament = departaments[indexPath.row]
        }
        
        cell.departamentLabel.text = departament!.name
        cell.zoneLabel.text = departament!.zone
        
        return cell
    }
    
   /* override func viewWillAppear(_ animated: Bool) {
        if splitViewController!.isCollapsed {
            if let selectionIndexPath = tableView.indexPathForSelectedRow {
                tableView.deselectRow(at: selectionIndexPath, animated: animated)
            }
        }
        super.viewWillAppear(animated)
    }*/
    
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier ?? "") {
        case "municipalities":
            guard let municipalitiesControllers = segue.destination as? MunicipalitiesTableViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            
            guard let selectedCell = sender as? DepartamentsTableViewCell else {
                fatalError("Unexpected sender")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selectd cell is ...")
            }
            
            let selectedDepartament: Departament?
            
            if isFiltering() {
                selectedDepartament = filteredDepartaments[indexPath.row]
            } else {
                selectedDepartament = departaments[indexPath.row]
            }
            
            municipalitiesControllers.municipalities = selectedDepartament!.municipalities!
            break
            
        default:
            fatalError("Unexpected segue")
        }
    }
    

    func filterContentForSearchText(_ searchText: String) {
        let scope: String = "All"
        filteredDepartaments = departaments.filter({( departament : Departament) -> Bool in
            let doesCategoryMatch = (scope == "All") || (departament.name == scope)
            
            if searchBarIsEmpty() {
                return doesCategoryMatch
            } else {
                return doesCategoryMatch && (departament.name?.lowercased().contains(searchText.lowercased()))!
                
                //return doesCategoryMatch
            }
        })
        tableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
}


extension DepartamentTableViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        //filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
        filterContentForSearchText(searchBar.text!)
    }
}

extension DepartamentTableViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        //let searchBar = searchController.searchBar
        //let scope = searchBar.scopeButtonTitles?[searchBar.selectedScopeButtonIndex]
        
        //print(scope)
        print(searchController.searchBar.text!)
        //filterContentForSearchText(searchController.searchBar.text!, scope: scope!)
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
