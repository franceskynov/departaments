//
//  Municipipality.swift
//  Departaments
//
//  Created by Dev on 11/1/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation

final class Municipality: NSObject {
    
    var id: Int?
    var name: String?
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as? Int
        self.name = dictionary["name"] as? String
    }
}
