//
//  Departaments.swift
//  Departaments
//
//  Created by Dev on 11/1/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation

class Departament: NSObject {
    
    var id: Int?
    var name: String?
    var zone: String?
    var municipalities: [Municipality]?
    
    init(id: Int, name: String, zone: String, municipalities: [Municipality]) {
        self.id = id
        self.name = name
        self.zone = zone
        self.municipalities = municipalities
    }
    
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as? Int
        self.name = dictionary["name"] as? String
        self.zone = dictionary["zone"] as? String
        self.municipalities = dictionary["municipalities"] as? [Municipality]
    }
}
